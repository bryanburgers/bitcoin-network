use async_std::{
    net::{ToSocketAddrs},
    task,
};
use clap::{App, Arg};
use std::net::SocketAddr;

mod agent;
mod broker;
mod message;

use agent::Agent;
use broker::BrokerHandle;

fn main() -> Result<(), std::io::Error> {
    let matches = App::new("bitcoin-network")
        .version("1.0")
        .author("Bryan Burgers <bryan@burgers.io>")
        .about("Tries to crawl the bitcoin network")
        .arg(Arg::with_name("address").required(true))
        .get_matches();

    // cargo +beta run testnet-seed.bitcoin.jonasschnelli.ch:18333
    let address = matches.value_of("address").unwrap();

    let fut = run(address);
    task::block_on(fut)
}

async fn run_one(address: SocketAddr, handle: BrokerHandle) -> Result<(), std::io::Error> {
    println!("Connecting to {}", address);
    Agent::connect(message::Network::Testnet3, address, handle).await
}

async fn run(address: impl ToSocketAddrs) -> Result<(), std::io::Error> {
    let (broker, handle_generator) = broker::Broker::new();
    // task::spawn(broker.run());

    let addrs = address.to_socket_addrs().await?;

    for addr in addrs {
        let broker_handle = handle_generator.get_handle(addr);
        task::spawn(run_one(addr, broker_handle));

        /*
        println!("Connecting to {}...", addr);
        match TcpStream::connect(addr).await {
            Ok(stream) => {
                println!("Connected!");
                //task::spawn(run_one(stream));
                run_one(stream).await?;
                //break;
                println!();
            },
            Err(err) => {
                println!("Failed: {:?}", err);
                println!();
            },
        }
        */
    }

    println!("Spawning broker");
    broker.run().await?;

    Ok(())
}
