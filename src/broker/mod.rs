use futures_channel::mpsc;
use std::net::SocketAddr;
use futures_util::sink::SinkExt;
use async_std::prelude::*;
use std::collections::HashSet;

pub struct Broker {
    receiver: mpsc::UnboundedReceiver<BrokerMessage>,
}

impl Broker {
    pub fn new() -> (Broker, BrokerHandleGenerator) {
        let (sender, receiver) = mpsc::unbounded();
        let broker = Broker {
            receiver,
        };
        let handle_generator = BrokerHandleGenerator {
            sender,
        };
        (broker, handle_generator)
    }

    pub async fn run(mut self) -> Result<(), std::io::Error> {
        let mut connected = 0;
        let mut seen = HashSet::new();


        while let Some(message) = self.receiver.next().await {
            match message {
                BrokerMessage::Connected { address, .. } => {
                    connected += 1;
                    println!("{}: connected. Total connected is now {}", address, connected);
                }
                BrokerMessage::NewAddresses { address, addresses } => {
                    let mut new = 0;
                    let mut repeat = 0;

                    for address in addresses {
                        match seen.insert(address) {
                            true => { new += 1 },
                            false => { repeat += 1},
                        }
                    }

                    println!("{}: saw {} addresses ({} new, {} repeats)", address, new + repeat, new, repeat);
                    println!("Total seen: {}, connected: {}", seen.len(), connected);
                }
                BrokerMessage::Disconnected { address } => {
                    connected -= 1;
                    println!("{}: disconnected. Total connected is now {}", address, connected);
                }
            }
        }

        Ok(())
    }
}

pub struct BrokerHandleGenerator {
    sender: mpsc::UnboundedSender<BrokerMessage>,
}

impl BrokerHandleGenerator {
    pub fn get_handle(&self, address: SocketAddr) -> BrokerHandle {
        BrokerHandle { address, sender: self.sender.clone() }
    }
}


enum BrokerMessage {
    Connected {
        address: SocketAddr,
        user_agent: String,
        version: i32,
    },
    NewAddresses {
        address: SocketAddr,
        addresses: Vec<SocketAddr>,
    },
    Disconnected {
        address: SocketAddr,
    },
}

#[derive(Clone)]
pub struct BrokerHandle {
    address: SocketAddr,
    sender: mpsc::UnboundedSender<BrokerMessage>,
}

impl BrokerHandle {
    pub async fn connected(&mut self, user_agent: String, version: i32) {
        self.sender.send(BrokerMessage::Connected {
            address: self.address,
            user_agent,
            version,
        }).await.unwrap();
    }

    pub async fn new_addresses(&mut self, addresses: Vec<SocketAddr>) {
        self.sender.send(BrokerMessage::NewAddresses {
            address: self.address,
            addresses,
        }).await.unwrap()
    }

    pub async fn disconnected(&mut self) {
        self.sender.send(BrokerMessage::Disconnected {
            address: self.address,
        }).await.unwrap()
    }
}
