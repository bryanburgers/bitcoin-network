use crate::broker::BrokerHandle;
use async_std::{
    sync::Arc,
    net::TcpStream,
    future,
    prelude::*,
};
use futures_channel::mpsc;
use futures_util::sink::SinkExt;
use futures_util::select;
use futures_util::FutureExt;
use crate::message::{Message, MessagePayload, Network, StreamReader, StreamWriter};
use crate::message::messages::*;
use crate::message;
use std::time::SystemTime;
use std::net::SocketAddr;

type Sender<T> = mpsc::UnboundedSender<T>;
type Receiver<T> = mpsc::UnboundedReceiver<T>;

/// The agent for a single connection
pub struct Agent {
}

enum Void {}

impl Agent {
    pub async fn connect(network: Network, address: SocketAddr, broker_handle: BrokerHandle) -> Result<(), std::io::Error> {
        let stream = TcpStream::connect(address).await?;

        let agent = Agent {};
        agent.run(network, stream, broker_handle).await
    }

    async fn run(self, network: Network, stream: TcpStream, broker_handle: BrokerHandle) -> Result<(), std::io::Error> {
        let stream = Arc::new(stream);
        let (shutdown_tx, shutdown_rx) = mpsc::unbounded();
        let (read_agent_tx, read_agent_rx) = mpsc::unbounded();
        let (agent_write_tx, agent_write_rx) = mpsc::unbounded();
        let read_future = self.run_reader(network.clone(), stream.clone(), read_agent_tx, shutdown_rx);
        let write_future = self.run_writer(network.clone(), stream.clone(), agent_write_rx);
        let agent_future = self.run_agent(stream.clone(), broker_handle, read_agent_rx, agent_write_tx, shutdown_tx);

        let future = future::try_join!(read_future, write_future, agent_future);

        future.await?;

        Ok(())
    }

    async fn run_agent(&self, stream: Arc<TcpStream>, mut broker_handle: BrokerHandle, mut receiver: Receiver<Message>, mut sender: Sender<Message>, _shutdown: Sender<Void>) -> Result<(), std::io::Error> {

        let timestamp: i64 = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() as i64;
        let peer_addr = stream.peer_addr()?;
        let peer_ip = match peer_addr.ip() {
            std::net::IpAddr::V4(a) => a.to_ipv6_mapped(),
            std::net::IpAddr::V6(a) => a,
        };
        let local_addr = stream.local_addr()?;
        let local_ip = match local_addr.ip() {
            std::net::IpAddr::V4(a) => a.to_ipv6_mapped(),
            std::net::IpAddr::V6(a) => a,
        };

        let message = Message::version(Version {
            version: 70015,
            services: 1,
            timestamp,
            nonce: 0,
            addr_recv: message::component::Address {
                services: 1,
                ip: peer_ip,
                port: peer_addr.port(),
            },
            addr_from: message::component::Address {
                services: 1,
                ip: local_ip,
                port: local_addr.port(),
            },
            user_agent: "/bitcoin-network-crawler-test/".into(),
            start_height: 0,
            relay: false,
        });
        sender.send(message).await.unwrap();

        while let Some(message) = receiver.next().await {
            match message.payload {
                MessagePayload::Version(version) => { 
                    broker_handle.connected(version.user_agent, version.version).await;
                    sender.send(Message::verack()).await.unwrap();
                }
                MessagePayload::VerAck(_) => {
                    sender.send(Message::getaddr()).await.unwrap();
                }
                MessagePayload::Ping(ping) => {
                    sender.send(Message::pong(ping.nonce)).await.unwrap();
                }
                MessagePayload::Addr(addr) => {
                    let addresses = addr.addresses.into_iter().map(|(_timestamp, address)| {
                        std::net::SocketAddr::new(address.ip.into(), address.port)
                    }).collect();
                    broker_handle.new_addresses(addresses).await;
                }
                MessagePayload::Unknown(_) => {
                    /* Log this? Nah. */
                }
                _ => {},
            }
        }

        broker_handle.disconnected().await;

        Ok(())
    }

    async fn run_reader(&self, network: Network, stream: Arc<TcpStream>, mut sender: Sender<Message>, mut shutdown: Receiver<Void>) -> Result<(), std::io::Error> {
        let stream = &*stream;
        let mut stream_reader = StreamReader::new(network, stream);

        loop {
            select! {
                message = stream_reader.next().fuse() => match message {
                    Some(message) => match message {
                        Ok(message) => {
                            println!("↓ {}", message.command.as_str());
                            sender.send(message).await.unwrap()
                        }
                        Err(err) => {
                            println!("Agent::run_reader got an error! {:?}", err);
                        }
                    }
                    None => break,
                },
                void = shutdown.next().fuse() => match void {
                    Some(void) => match void {},
                    None => break
                },
            }
        }

        Ok(())
    }

    async fn run_writer(&self, network: Network, stream: Arc<TcpStream>, mut receiver: Receiver<Message>) -> Result<(), std::io::Error> {
        // WHAT!? The async-std book did this. I'm not sure if they had some invarients they were
        // keeping to make this a valid move.
        let stream = &*stream;
        let mut stream_writer = StreamWriter::new(network, stream);

        while let Some(message) = receiver.next().await {
            println!("↑ {}", message.command.as_str());
            stream_writer.send_actual_message(message).await?;
        }

        Ok(())
    }
}
