use sha2::{Digest, Sha256};
use futures::io::AsyncWrite;
use futures::io::AsyncWriteExt;
//use async_std::prelude::*;

pub mod component;
pub mod messages;
mod stream_reader;

use component::*;
pub use messages::*;
pub use stream_reader::StreamReader;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Message {
    pub command: MessageName,
    pub payload: MessagePayload,
}

impl Message {
    pub fn version(version: messages::Version) -> Self {
        Message {
            command: messages::Version::COMMAND.into(),
            payload: MessagePayload::Version(version),
        }
    }

    pub fn verack() -> Self {
        Message {
            command: messages::VerAck::COMMAND.into(),
            payload: MessagePayload::VerAck(messages::VerAck),
        }
    }

    /*
    pub fn ping(nonce: u64) -> Self {
        Message {
            command: messages::Ping::COMMAND.into(),
            payload: MessagePayload::Ping(messages::Ping { nonce }),
        }
    }
    */

    pub fn pong(nonce: u64) -> Self {
        Message {
            command: messages::Pong::COMMAND.into(),
            payload: MessagePayload::Pong(messages::Pong { nonce }),
        }
    }

    pub fn getaddr() -> Self {
        Message {
            command: messages::GetAddr::COMMAND.into(),
            payload: MessagePayload::GetAddr(messages::GetAddr),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub enum MessagePayload {
    Version(messages::Version),
    VerAck(messages::VerAck),
    Alert(messages::Alert),
    Addr(messages::Addr),
    Ping(messages::Ping),
    Pong(messages::Pong),
    GetAddr(messages::GetAddr),
    Unknown(Vec<u8>),
}

impl MessagePayload {
    fn to_bytes(&self, data: &mut Vec<u8>) {
        match self {
            Self::Version(version) => version.to_bytes(data),
            Self::VerAck(verack) => verack.to_bytes(data),
            Self::Alert(alert) => alert.to_bytes(data),
            Self::Addr(addr) => addr.to_bytes(data),
            Self::Ping(ping) => ping.to_bytes(data),
            Self::Pong(pong) => pong.to_bytes(data),
            Self::GetAddr(getaddr) => getaddr.to_bytes(data),
            Self::Unknown(unknown) => { std::io::Write::write(data, &unknown[..]).unwrap(); },
        }
    }
}

#[derive(Debug, Clone)]
pub enum Network {
    Main,
    Testnet,
    Testnet3,
    Namecoin,
    // Unknown(u32),
}

impl Network {
    fn id(&self) -> u32 {
        match *self {
            Self::Main => 0xd9b4bef9,
            Self::Testnet => 0xdab5bffa,
            Self::Testnet3 => 0x0709110b,
            Self::Namecoin => 0xfeb4bef9,
            // Self::Unknown(ref id) => *id,
        }
    }
}

impl MessageComponent for Network {
    fn from_bytes(&mut self, _data: &[u8]) -> Option<usize> {
        // TODO: Parse this!
        None
    }

    fn to_bytes(&self, stream: &mut impl std::io::Write) {
        stream.write(&self.id().to_le_bytes()).unwrap();
    }
}

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct MessageName([u8; 12]);

impl MessageName {
    pub fn as_str(&self) -> &str {
        let mut first_null = 12;
        for i in 0..12 {
            if self.0[i] == 0 {
                first_null = i;
                break;
            }
        }
        unsafe { std::str::from_utf8_unchecked(&self.0[0..first_null]) }
    }
}

impl From<String> for MessageName {
    fn from(s: String) -> Self {
        let mut v: Vec<u8> = s.into_bytes();
        while v.len() < 12 {
            v.push(0);
        }
        let mut a: [u8; 12] = Default::default();
        a.copy_from_slice(&v[0..12]);
        MessageName(a)
    }
}

impl From<&[u8; 12]> for MessageName {
    fn from(v: &[u8; 12]) -> Self {
        let mut a: [u8; 12] = Default::default();
        a.copy_from_slice(&v[0..12]);
        MessageName(a)
    }
}

impl MessageComponent for MessageName {
    fn from_bytes(&mut self, _data: &[u8]) -> Option<usize> {
        // TODO: parse
        None
    }

    fn to_bytes(&self, stream: &mut impl std::io::Write) {
        stream.write(&self.0).unwrap();
    }
}

//main  0xD9B4BEF9  F9 BE B4 D9
//testnet   0xDAB5BFFA  FA BF B5 DA
//testnet3  0x0709110B  0B 11 09 07
//namecoin  0xFEB4BEF9  F9 BE B4 FE

/// Write messages to a stream
pub struct StreamWriter<T> {
    network: Network,
    stream: T,
}

impl<T> StreamWriter<T>
where 
    T: AsyncWrite,
    T: Unpin,
{
    pub fn new(network: Network, stream: T) -> Self {
        StreamWriter { network, stream }
    }

    pub async fn send_actual_message(&mut self, message: Message) -> Result<(), std::io::Error> {
        let mut full_bytes = Vec::new();
        let mut message_bytes = Vec::new();

        message.payload.to_bytes(&mut message_bytes);

        self.network.to_bytes(&mut full_bytes);
        std::io::Write::write(&mut full_bytes, &message.command.0).unwrap();
        (message_bytes.len() as u32).to_bytes(&mut full_bytes);

        let mut sha = Sha256::new();
        sha.input(&message_bytes[..]);
        let output = sha.result_reset();
        sha.input(&output);
        let output = sha.result();
        std::io::Write::write(&mut full_bytes, &output[0..4]).unwrap();

        std::io::Write::write(&mut full_bytes, &message_bytes[..]).unwrap();

        self.stream.write_all(&full_bytes[..]).await?;

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_version() {
        let version = Version {
            version: 31900,
            services: 1,
            timestamp: 0x4d1015e6,
            addr_recv: Address {
                services: 1,
                ip: "::ffff:10.0.0.1".parse().unwrap(),
                port: 36128,
            },
            addr_from: Address {
                services: 1,
                ip: "::ffff:10.0.0.2".parse().unwrap(),
                port: 36128,
            },
            nonce: 0x1357b43a2c209ddd,
            user_agent: "".to_string(),
            start_height: 98645,
            relay: false,
        };
        let mut received = Vec::new();
        let expected = vec![
            0x9C, 0x7C, 0x00, 0x00, // - 31900 (version 0.3.19)
            0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // - 1 (NODE_NETWORK services)
            0xE6, 0x15, 0x10, 0x4D, 0x00, 0x00, 0x00, 0x00, // - Mon Dec 20 21:50:14 EST 2010
            0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x0A, 0x00, 0x00, 0x01, 0x20,
            0x8D, // - Recipient address info - see Network Address
            0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x0A, 0x00, 0x00, 0x02, 0x20,
            0x8D, // - Sender address info - see Network Address
            0xDD, 0x9D, 0x20, 0x2C, 0x3A, 0xB4, 0x57, 0x13, // - Node random unique ID
            0x00, // - "" sub-version string (string is 0 bytes long)
            0x55, 0x81, 0x01, 0x00, // - Last block sending node has is block #98645
            0x00, // - Relay
        ];
        version.to_bytes(&mut received);
        assert_eq!(&received[..], &expected[..]);
    }
}
